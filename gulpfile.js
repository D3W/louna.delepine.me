'use strict'

const gulp = require('gulp');
const sass = require('gulp-sass')(require('sass'));
const stylelint = require('@ronilaukkarinen/gulp-stylelint');
const autoprefixer = require('gulp-autoprefixer');

function buildStyles() {
  return gulp.src('./scss/**/*.scss')
    .pipe(sass())
    .pipe(autoprefixer({

    }))
    // .pipe(stylelint({
    //   fix: true,
    //   failAfterError: true,
    //   reportOutputDir: 'reports/lint',
    //   reporters: [
    //     {formatter: 'json', save: 'report.json'},
    //   ],
    //   debug: true
    // }))
    .pipe(gulp.dest('./assets/css'));

}

exports.build = buildStyles;
exports.watch = function () {
  gulp.watch('./scss/**/*.scss', gulp.series('build'))
}
